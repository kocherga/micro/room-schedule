#include <main.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <HardwareSerial.h>
#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(24, 12, NEO_GRB + NEO_KHZ800);

HTTPClient http;
WiFiClient w_client;
StaticJsonDocument<2022> doc;

void set_pixel(uint hour, uint32_t color) {
  int position;
  if (hour % 2)
    position = (11 - hour / 2) + 12;
  else
    position = hour / 2;  
  pixels.setPixelColor(position, color);

}

void setup() {
  WiFi.begin("Kocherga", "Wittgenstein");
  Serial.begin(9600);
  Serial.println("Connecting...");
  WiFi.waitForConnectResult();
  Serial.println();
  if (WiFi.isConnected()) {
    Serial.println("Connection established.");
  }

  pixels.begin();
  pixels.show();
  for (int i = 0; i < 24; i++) {
    set_pixel(i, 0x090909);
    pixels.show();
    delay(100);
  }
  
}

int hour_statuses[24];

// parses N chars from buffer as int.
int read_int(int chars, const char* from) {
  int accum = 0;
  for (int i = 0; i < chars; i++)
    accum = accum * 10 + (from[i] - '0');
  return accum;
}

void loop() {
  http.begin(w_client, HOST, PORT, METHOD);
  http.GET();
  deserializeJson(doc, http.getString());
  http.end();

  // This is full of potential overflows.
  int now = doc["now"].as<int>();
  auto hours = doc["hours"].as<JsonArray>();

  for (int i = 0; i < 24; i++) {
    if (i == now)
      set_pixel(i, 0x002f00);
    else
      set_pixel(i, 0x000800);
  }
  for (unsigned int i = 0; i < hours.size(); i++) {
    int hour = hours[i].as<int>();
    if (hour == now)
      set_pixel(hour, 0x2f0000);
    else
      set_pixel(hour, 0x080000);
  }
  pixels.show();

  delay(10000);
}