#!/bin/env python
import sanic
from sanic import Sanic
from sanic.response import json as json_response
import json
import aiohttp
from datetime import datetime, date

app = Sanic()
rooms = {
    "occupied": "Летняя",
    "L2": "ГЭБ",
    "cold": "Лекционная",
    "tea": "Китайская"
}

def isoparse_compat(time):
    return datetime.strptime(time[:19] + time[19:22] + time[23:25], "%Y-%m-%dT%H:%M:%S%z")

@app.route("/hackme/rooms")
async def get_rooms(rq):
    return json_response(rooms)


@app.route("/hackme/bookings")
async def get_bookings(rq):
    query = rq.query_string
    if query not in rooms:
        return json_response({"msg": "room not found"})
    query = rooms[query]

    async with aiohttp.ClientSession() as session:
        async with session.get("https://api.kocherga.club/api/bookings/today") as response:
            bookings = json.loads(await response.text())
            taken_hours = set()

            now = datetime.now()
            today = now.date()
            current_hour = datetime.now().hour

            for booking in bookings:
                if (booking["room"] != query):
                    continue

                start = isoparse_compat(booking["start"])
                end = datetime.fromtimestamp(isoparse_compat(booking["end"]).timestamp() - 1)
                starth = start.hour
                endh = end.hour

                if start.date() < today:
                    starth = 0
                if end.date() > today:
                    endh = 23

                taken_hours |= set(range(starth, endh + 1))

            # we'll transform response to a more simple model.
            return json_response({
                "hours": list(taken_hours),
                "now": current_hour
            })

app.run(host='0.0.0.0', port=8889)
